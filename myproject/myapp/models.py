from django.contrib.auth.models import AbstractUser
from djongo import models

class CustomUser(AbstractUser):
    userid = models.AutoField(primary_key=True)
    email = models.EmailField(max_length=255, unique=True)
    password = models.CharField(max_length=255)
    usernumble = models.IntegerField(null=True)
    date_joined = models.DateField(auto_now_add=True)
    refresh_token = models.TextField(null=True)
    is_login = models.BooleanField(default=False)
    ADMIN = 'admin'
    USER = 'user'
    ROLE_CHOICES = [
        (ADMIN, 'Admin'),
        (USER, 'User'),
    ]

    role = models.CharField(
        max_length=50,
        choices=ROLE_CHOICES,
        default=USER,
    )

    def __str__(self):
        return f"{self.userid}"
        
class Master(models.Model):
    id = models.AutoField(primary_key=True)
    category = models.CharField(max_length=255)
    created_date = models.DateTimeField(auto_now_add=True)
    updated_date = models.DateTimeField(auto_now=True)
    
    def __str__(self):
        return f"{self.id}"
    
class Detail(models.Model):
    uid = models.AutoField(primary_key=True)
    master = models.ForeignKey(Master, on_delete=models.CASCADE)
    name = models.CharField(max_length=255)
    SHOWN = 'True'
    NOTSHOWN = 'False'
    SHOWN_CHOICES = [
        (SHOWN, 'True'),
        (NOTSHOWN, 'False'),
    ]

    isshown = models.CharField(
        max_length=50,
        choices=SHOWN_CHOICES,
        default=SHOWN,
    )
    
class LeadCenter(models.Model):
    LeadCenter_id = models.AutoField(primary_key=True)
    Handle_by = models.ForeignKey(CustomUser,on_delete=models.CASCADE)
    PhoneNumber = models.IntegerField(null=True)
    Name = models.CharField(max_length=255, null=True)
    
    class LeadStatus(models.TextChoices):
        NEW = 'New', 'New Lead'
        IN_PROGRESS = 'In Progress', 'In Progress Lead'
        COMPLETED = 'Completed', 'Completed Lead'
        CANCELLED = 'Cancelled', 'Cancelled Lead'

    lead_status = models.CharField(
        max_length=20,
        choices=LeadStatus.choices,
        default=LeadStatus.NEW,
    )
    Note = models.CharField(max_length=255,null=True)
    Proccessdoneat = models.DateTimeField(auto_now_add=True)
    Tags = models.CharField(max_length=255,null=True)
    
    
class LeadDetail(models.Model):
    LeadDetail_id = models.AutoField(primary_key=True)
    Cus_name = models.ForeignKey(LeadCenter,on_delete=models.CASCADE)
    uid = models.ForeignKey(Detail,on_delete=models.CASCADE)
    Quantity = models.IntegerField(null=True)
    order_id = models.IntegerField(null=True)
    Is_new_customer = models.BooleanField(default=True)
    Is_new_existed = models.BooleanField(default=True)
    Is_dupplicated = models.BooleanField(default=False)