from drf_yasg import openapi
from rest_framework import permissions

def get_swagger_schema_view():
    schema_view = openapi.Info(
        title='MyProject API',
        default_version='v1',
        description='API for MyProject',
        terms_of_service='https://www.example.com/terms/',
        contact=openapi.Contact(email='Test@example.com'),
        license=openapi.License(name='BSD License'),
    )

    schema_view.get_schema = permissions.AllowAny()

    return schema_view