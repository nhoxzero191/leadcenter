from django.urls import path
from .views import UserList, UserDetail, UserLogin, TokenRefresh, MasterList, MasterDetail, DetailList, DetailDetail , LeadCenterDetail,LeadCenterList,LeadCenter
from django.urls import path
from rest_framework.authtoken.views import obtain_auth_token


urlpatterns = [
    path('api/v1/users/', UserList.as_view(), name='user-list'),
    path('api/v1/users/<int:pk>/', UserDetail.as_view(), name='user-detail'),
    path('api/v1/login/', UserLogin.as_view(), name='user-login'),
    path('api/v1/refresh/', TokenRefresh.as_view(), name='token-refresh'),
    path('api/v1/masters/', MasterList.as_view(), name='master-list'),
    path('api/v1/masters/<int:pk>/', MasterDetail.as_view(), name='master-detail'),
    path('api/v1/details/', DetailList.as_view(), name='detail-list'),
    path('api/v1/details/<int:pk>/', DetailDetail.as_view(), name='detail-detail'),
    path('api/v1/leadcenter/', LeadCenterList.as_view(), name='leadcenter-list'),
    path('api/v1/leadcenter/<int:pk>/', LeadCenterDetail.as_view(), name='leadcenter-detail'),
    
]
