# app/views.py

from django.contrib.auth import authenticate
from django.http import Http404
from drf_yasg import openapi
from drf_yasg.utils import swagger_auto_schema
from rest_framework import status
from rest_framework.response import Response
from rest_framework.views import APIView
from rest_framework.permissions import IsAdminUser
from rest_framework_simplejwt.tokens import RefreshToken
from .models import CustomUser , Master ,Detail , LeadCenter ,LeadDetail
from.serializers import UserSerializer, MasterSerializer, DetailSerializer, LeadCenterSerializer , LeadDetailSerializer
from rest_framework import generics


class UserList(generics.ListCreateAPIView): # type: ignore
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

class UserDetail(generics.RetrieveUpdateDestroyAPIView): # type: ignore
    queryset = CustomUser.objects.all()
    serializer_class = UserSerializer

class UserLogin(APIView):
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "email": openapi.Schema(type=openapi.TYPE_STRING),
                "password": openapi.Schema(type=openapi.TYPE_STRING),
            },
            required=['email', 'password']
        )
    )
    def post(self, request, format=None):
        email = request.data.get('email')
        password = request.data.get('password')

        try:
            user = CustomUser.objects.get(email=email)
            if user.check_password(password):
                refresh = RefreshToken.for_user(user)
                return Response({
                    'message': 'Login successful',
                    'user': UserSerializer(user).data,
                    'access_token': str(refresh.access_token), # type: ignore
                    'refresh_token': str(refresh)
                }, status=status.HTTP_200_OK)
            else:
                return Response({'detail': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)
        except CustomUser.DoesNotExist:
            return Response({'detail': 'Invalid credentials'}, status=status.HTTP_401_UNAUTHORIZED)

class TokenRefresh(APIView):
    @swagger_auto_schema(
        request_body=openapi.Schema(
            type=openapi.TYPE_OBJECT,
            properties={
                "refresh": openapi.Schema(type=openapi.TYPE_STRING),
            },
            required=['refresh']
        )
    )
    def post(self, request, format=None):
        refresh_token = request.data.get('refresh')
        try:
            refresh = RefreshToken(refresh_token)
            return Response({
                'access_token': str(refresh.access_token)
            }, status=status.HTTP_200_OK)
        except Exception as e:
            return Response({'detail': 'Invalid refresh token'}, status=status.HTTP_401_UNAUTHORIZED)

class MasterList(generics.ListCreateAPIView):
    queryset = Master.objects.all()
    serializer_class = MasterSerializer

class MasterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Master.objects.all()
    serializer_class = MasterSerializer

class DetailList(generics.ListCreateAPIView):
    queryset = Detail.objects.all()
    serializer_class = DetailSerializer

class DetailDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = Detail.objects.all()
    serializer_class = DetailSerializer
    
class LeadCenterList(generics.ListCreateAPIView):
    queryset = LeadCenter.objects.all()
    serializer_class = LeadCenterSerializer

class LeadCenterDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = LeadCenter.objects.all()
    serializer_class = LeadCenterSerializer
    
class LeadDetailList(generics.ListCreateAPIView):
    queryset = LeadDetail.objects.all()
    serializer_class = LeadDetailSerializer

class LeadDetailDetail(generics.RetrieveUpdateDestroyAPIView):
    queryset = LeadDetail.objects.all()
    serializer_class = LeadDetailSerializer