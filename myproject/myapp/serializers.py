from rest_framework import serializers
from .models import CustomUser, Master, Detail, LeadDetail, LeadCenter
from rest_framework_simplejwt.tokens import RefreshToken

class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = CustomUser
        fields = ['userid', 'username', 'email', 'password', 'usernumble', 'role']
        extra_kwargs = {
            'password': {'write_only': True}
        }

    def create(self, validated_data):
        user = CustomUser.objects.create_user( # type: ignore
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
            usernumble=validated_data['usernumble'],
            role=validated_data['role']
        )
        return user

    def update(self, instance, validated_data):
        if 'password' in validated_data:
            instance.set_password(validated_data['password'])
            validated_data.pop('password')
        return super().update(instance, validated_data)


class UserRegisterSerializer(serializers.ModelSerializer):
    password = serializers.CharField(write_only=True)

    class Meta:
        model = CustomUser
        fields = ['username', 'email', 'password', 'usernumble']

    def create(self, validated_data):
        user = CustomUser.objects.create(
            username=validated_data['username'],
            email=validated_data['email'],
            password=validated_data['password'],
            first_name=validated_data['usernumble'],
        )
        return user

class UserLoginSerializer(serializers.Serializer):
    email = serializers.EmailField()
    password = serializers.CharField()
    
class MasterSerializer(serializers.ModelSerializer):
    class Meta:
        model = Master
        fields = "__all__"
        read_only_fields = (
            "id",
            "created_at",
            "updated_at",
        )

    def create(self, validated_data):
        master = Master.objects.create(**validated_data)
        return master
    
class DetailSerializer(serializers.ModelSerializer):
    master = MasterSerializer()

    class Meta:
        model = Detail
        fields = "__all__"
        read_only_fields = (
            "detail_id",
        )

    def create(self, validated_data):
        master_data = validated_data.pop('master')
        master_category = master_data.get('category')

        master, created = Master.objects.get_or_create(category=master_category)
        detail = Detail.objects.create(master=master, **validated_data)
        return detail
    
class LeadCenterSerializer(serializers.ModelSerializer):
    Handle_by = serializers.PrimaryKeyRelatedField(queryset=CustomUser.objects.all())

    class Meta:
        model = LeadCenter
        fields = ['LeadCenter_id', 'Handle_by', 'PhoneNumber', 'Name', 'lead_status', 'Note', 'Proccessdoneat', 'Tags']

    def to_representation(self, instance):
        representation = super().to_representation(instance)
        handle_by_user = CustomUser.objects.get(userid=representation['Handle_by'])
        user_serializer = UserSerializer(handle_by_user)
        representation['Handle_by'] = user_serializer.data
        return representation

class LeadDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = LeadDetail
        fields = ['LeadDetail_id', 'Cus_mname', 'uid', 'Quantity', 'order_id', 'Is_new_customer', 'Is_new_existed', 'Is_dupplicated']
